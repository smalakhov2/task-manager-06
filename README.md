# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME:** Sergei Malakhov

**E-MAIL:** smalakhov2@rencredit.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10 x64

#HARDWARE
- HDD 200ГБ
- CPU Intel Celeron 2.16GHz

# PROGRAM BUILD
```bash
gradle clean build
```

# PROGRAM RUN
```bash
java -jar ./task-manager-1.0.2.jar
```
