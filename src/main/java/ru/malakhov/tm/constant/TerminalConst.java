package ru.malakhov.tm.constant;

public interface TerminalConst {

    String HELP = "help";
    String ABOUT = "about";
    String VERSION = "version";

}
